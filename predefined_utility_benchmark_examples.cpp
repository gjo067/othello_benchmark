// gtest
#include <benchmark/benchmark.h>   // googletest header file

// cc library
#include <engine.h>
#include "minimax_ai.h"
#include <iostream>

namespace detail
{

  // Declarations
  auto generateStartPositionBitBoard();

  std::vector<double> generateSameWeights(){
      return {
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1,
          1, 1, 1, 1, 1, 1, 1, 1
      };
  }

  // Definitions
  auto generateStartPositionBitBoard()
  {

    const othello::BitPieces white_starter_pieces{"00000000"
                                                  "00000000"
                                                  "00000000"
                                                  "00010000"
                                                  "00001000"
                                                  "00000000"
                                                  "00000000"
                                                  "00000000"};
    const othello::BitPieces black_starter_pieces{"00000000"
                                                  "00000000"
                                                  "00000000"
                                                  "00001000"
                                                  "00010000"
                                                  "00000000"
                                                  "00000000"
                                                  "00000000"};

    return othello::BitBoard{white_starter_pieces, black_starter_pieces};
  }

  auto generateCrowdedBoard() {
      const othello::BitPieces whitePieces {
          "00000000"
          "00001000"
          "11101000"
          "01001100"
          "01111110"
          "00001110"
          "00000100"
          "00000000"
          "00000000"
      };

      const othello::BitPieces blackPieces {
          "00000000"
          "00000000"
          "00000000"
          "00110000"
          "00000000"
          "00110001"
          "00101010"
          "00100101"
      };

      return othello::BitBoard{whitePieces, blackPieces};
  }
}   // namespace detail

static void leagalMovesFromInitBoard(benchmark::State& state)
{
  const auto bitboard = detail::generateStartPositionBitBoard();

  for (auto _ : state) {
    auto legal_moves
      = othello::utility::legalMoves(bitboard, othello::PlayerId::One);
    benchmark::DoNotOptimize(legal_moves);
  }
}

static void legalMovesOnCrowdedBoard(benchmark::State& state) {
    const auto bitboard = detail::generateCrowdedBoard();

    for(auto _ : state) {
        auto legalMoves = othello::utility::legalMoves(bitboard, othello::PlayerId::One);
        benchmark::DoNotOptimize(legalMoves);
    }
}

static void placeAndFlipOnInitBoard(benchmark::State& state) {
    auto board = detail::generateStartPositionBitBoard();
    const othello::BitPos boardPos{20};
    const othello::PlayerId playerId {othello::PlayerId::One};

    for(auto _ : state) {
        othello::utility::placeAndFlip(board, playerId, boardPos);
    }
}

static void placeAndFlipOnCrowdedBoard(benchmark::State& state) {
    auto board = detail::generateCrowdedBoard();
    const othello::BitPos boardPos {21};
    const othello::PlayerId playerId {othello::PlayerId::Two};

    for (auto _ : state) {
        othello::utility::placeAndFlip(board, playerId, boardPos);
    }
}

BENCHMARK(leagalMovesFromInitBoard);
BENCHMARK(legalMovesOnCrowdedBoard);
BENCHMARK(placeAndFlipOnInitBoard);
BENCHMARK(placeAndFlipOnCrowdedBoard);

BENCHMARK_MAIN();
